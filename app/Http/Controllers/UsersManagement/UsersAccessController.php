<?php

namespace App\Http\Controllers\UsersManagement;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UsersAccess as myData;
use App\Models\UsersAccess;
use App\Models\UsersLevel;
use App\Models\UsersMenu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UsersAccessController extends Controller
{
    var $table = "users_access";
    var $kodeMenu = "M999004";

    function index(Request $request)
    {
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
        if ($request->users_level_id) {
            $listUsersMenu = UsersMenu::from('users_menu')->select(DB::raw("users_menu.*, 
            (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $request->users_level_id and `access` = 'show') as `xshow`, 
            (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $request->users_level_id and `access` = 'add') as `xadd`, 
            (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $request->users_level_id and `access` = 'edit') as `xedit`, 
            (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $request->users_level_id and `access` = 'detail') as `xdetail`, 
            (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $request->users_level_id and `access` = 'delete') as `xdelete` 
            "))
            ->where("rel", 0)->where("is_deleted", 0)
            ->orderBy('sequence', 'asc')->get();
        }else{
            $listUsersMenu = UsersMenu::select(["*"])->where("rel", 0)->where("is_deleted", 0)
            ->orderBy('sequence', 'asc')->get();
        }
        $listUsersLevel = UsersLevel::select(["id", "name_level"])->where("is_deleted", 0)->get();
        $myUsersLevel = UsersLevel::whereId($request->users_level_id)->where("is_deleted", 0)->first();
        $data = array(
			'firstMenu' => 'M999',
			'secondMenu' => $this->kodeMenu,
            'listUsersMenu' => $listUsersMenu,
            'listUsersLevel' => $listUsersLevel,
            'myUsersLevel' => $myUsersLevel,
        );
        return view('users-access.index', $data);
    }

	public function GetUsersAccess_rel($rel='', $users_level_id='')
	{	
		return UsersMenu::from('users_menu')->select(DB::raw("users_menu.*, 
        (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $users_level_id and `access` = 'show') as `xshow`, 
        (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $users_level_id and `access` = 'add') as `xadd`, 
        (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $users_level_id and `access` = 'edit') as `xedit`, 
        (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $users_level_id and `access` = 'detail') as `xdetail`, 
        (select count(id) from `users_access` where `users_menu_id` = users_menu.id and `users_level_id` = $users_level_id and `access` = 'delete') as `xdelete` 
        "))
        ->where("rel", $rel)
        ->where("is_deleted", 0)
        ->orderBy('sequence', 'asc')->get();
	}

    public function where(Request $request)
    {
        $result['error'] = 3;
        $result['message'] = "Error Method";
        $result['status_code'] = 203;
        $status_validation = false;
        $validation_rules = [
            'id' => 'min:1|max:20|numeric',
            'limit' => 'max:1|numeric',
            'count' => 'max:1|numeric',
        ];
        $validator = Validator::make($request->all(), $validation_rules);
        if ($validator->fails()) {
            $result['error_validation'] = $validator->errors();
            $status_validation = true;
        }
        if ($status_validation == true) {
            $result['error'] = 1;
            $result['message'] = "Data is not found";
            return response()->json($result);
            exit;
        }
        $q = myData::select(["*"])->with(["created_by_user" => function ($query) {
            $query->select(["id", "name"]);
        }])->with("users_level")->with("users_menu");
        if ($request->id) {
            $q->where("id", $request->id);
        }
        $q = $q->get();
        if (count($q) > 0) {
            $result['error'] = 0;
            $result['message'] = "Successfully Read Data";
            $result['status_code'] = 202;
        } else {
            $result['error'] = 2;
            $result['message'] = "No data";
            $result['status_code'] = 202;
        }
        $result['total_data'] = count($q);
        $result['data'] = $q;
        return response()->json($result);
    }

    public function save(Request $request)
    {
        $result['error'] = 3;
        $result['message'] = "Error Requests";
        $result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'add') == null or $this->CheckAllowAccess($this->kodeMenu, 'edit') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to save";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
        $listUsersLevel = UsersLevel::where("id", $request->users_level_id)->first();
        if ($listUsersLevel == null) {
            $result['error'] = 2;
            $result['message'] = "Data Users Level Not Found";
            $result['status_code'] = 201;
        } else {
            //Access Show
            if (!empty($request->access_show)) {
                UsersAccess::whereNotIn('users_menu_id', $request->access_show)
                    ->whereAccess('show')
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
                foreach ($request->access_show as $value) {
                    $checkUsersAccess = UsersAccess::
                        where("users_level_id", $request->users_level_id)
                        ->where("users_menu_id", $value)
                        ->whereAccess('show')
                        ->exists();
                    if (!$checkUsersAccess) {
                        $data_insert = array(
                            'users_level_id' => $request->users_level_id,
                            'users_menu_id' => $value,
                            'access' => 'show',
                            'created_by' => Auth::user()->id,
                            'created_date' => $this->DateTime(),
                        );
                        UsersAccess::create($data_insert);
                    }
                }
            } else {
                UsersAccess::where("access", "show")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
            }

            //Access Add
            if (!empty($request->access_add)) {
                UsersAccess::whereNotIn('users_menu_id', $request->access_add)
                    ->where("access", "add")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
                foreach ($request->access_add as $value) {
                    $checkUsersAccess = UsersAccess::
                        where("users_level_id", $request->users_level_id)
                        ->where("users_menu_id", $value)
                        ->where("access", "add")
                        ->exists();
                    if (!$checkUsersAccess) {
                        $data_insert = array(
                            'users_level_id' => $request->users_level_id,
                            'users_menu_id' => $value,
                            'access' => 'add',
                            'created_by' => Auth::user()->id,
                            'created_date' => $this->DateTime(),
                        );
                        UsersAccess::create($data_insert);
                    }
                }
            } else {
                UsersAccess::where("access", "add")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
            }

            //Access Edit
            if (!empty($request->access_edit)) {
                UsersAccess::whereNotIn('users_menu_id', $request->access_edit)
                    ->where("access", "edit")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
                foreach ($request->access_edit as $value) {
                    $checkUsersAccess = UsersAccess::
                        where("users_level_id", $request->users_level_id)
                        ->where("users_menu_id", $value)
                        ->where("access", "edit")
                        ->exists();
                    if (!$checkUsersAccess) {
                        $data_insert = array(
                            'users_level_id' => $request->users_level_id,
                            'users_menu_id' => $value,
                            'access' => 'edit',
                            'created_by' => Auth::user()->id,
                            'created_date' => $this->DateTime(),
                        );
                        UsersAccess::create($data_insert);
                    }
                }
            } else {
                UsersAccess::where("access", "edit")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
            }

            //Access Detail
            if (!empty($request->access_detail)) {
                UsersAccess::whereNotIn('users_menu_id', $request->access_detail)
                    ->where("access", "detail")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
                foreach ($request->access_detail as $value) {
                    $checkUsersAccess = UsersAccess::
                        where("users_level_id", $request->users_level_id)
                        ->where("users_menu_id", $value)
                        ->where("access", "detail")
                        ->exists();
                    if (!$checkUsersAccess) {
                        $data_insert = array(
                            'users_level_id' => $request->users_level_id,
                            'users_menu_id' => $value,
                            'access' => 'detail',
                            'created_by' => Auth::user()->id,
                            'created_date' => $this->DateTime(),
                        );
                        UsersAccess::create($data_insert);
                    }
                }
            } else {
                UsersAccess::where("access", "detail")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
            }

            //Access Delete
            if (!empty($request->access_delete)) {
                UsersAccess::whereNotIn('users_menu_id', $request->access_delete)
                    ->where("access", "delete")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
                foreach ($request->access_delete as $value) {
                    $checkUsersAccess = UsersAccess::
                        where("users_level_id", $request->users_level_id)
                        ->where("users_menu_id", $value)
                        ->where("access", "delete")
                        ->exists();
                    if (!$checkUsersAccess) {
                        $data_insert = array(
                            'users_level_id' => $request->users_level_id,
                            'users_menu_id' => $value,
                            'access' => 'delete',
                            'created_by' => Auth::user()->id,
                            'created_date' => $this->DateTime(),
                        );
                        UsersAccess::create($data_insert);
                    }
                }
            } else {
                UsersAccess::where("access", "delete")
                    ->where("users_level_id", $request->users_level_id)
                    ->delete();
            }
            $result['error'] = 0;
            $result['message'] = "Successfully Save Data";
            $result['status_code'] = 201;
        }
		if ($result['error'] == 0) {
			Session::flash('success', $result['message']);
		}else{
			Session::flash('error', $result['message']);
		}
        return response()->json($result);
    }
}
