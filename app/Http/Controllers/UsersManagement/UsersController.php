<?php

namespace App\Http\Controllers\UsersManagement;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Users as myData;
use App\Models\UsersLevel;
use App\Models\UsersLevelGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{	
	var $table = "users";
	var $kodeMenu = "M999001";

	function index()
	{
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
		$data = array(
			'firstMenu' => 'M999',
			'secondMenu' => $this->kodeMenu,
			'access_add' => $this->CheckAllowAccess($this->kodeMenu, "add"),
			'access_edit' => $this->CheckAllowAccess($this->kodeMenu, "edit"),
			'access_delete' => $this->CheckAllowAccess($this->kodeMenu, "delete"),
			'listUsersLevel' => UsersLevel::select(["id", "name_level"])->where("is_deleted", 0)->get(),
		);
		return view('users.index', $data);
	}

	public function where(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Method";
		$result['status_code'] = 203;
		$status_validation = false;
		$validation_rules = [
            'id' => 'min:1|max:20|numeric',
            'using_users_level_group' => 'min:1|max:1|numeric',
			'limit' => 'max:1|numeric',
			'count' => 'max:1|numeric',
		];
		$validator = Validator::make($request->all(), $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
			return response()->json($result);exit;
		}
		// $q = myData::select(["*"])->with(["created_by_user" => function($query){
		// 	$query->select(["id", "name"]);
		// }]);
		$q = myData::select(["*"])->
		with(["created_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["last_modified_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["is_konfirmasi_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["perangkat_daerah" => function($query){
			$query->select(["id", "nama_perangkat_daerah"]);
		}])->with(["element_masyarakat" => function($query){
			$query->select(["id", "nama_element_masyarakat"]);
		}])->with(["users_level"]);
		if ($request->using_users_level_group == 1) {
			$q->with("users_level_group");
		}
		if ($request->id) {
			$q->where("id", $request->id);
		}
		$q = $q->get();
		if (count($q) > 0) {
			$result['error'] = 0;
			$result['message'] = "Successfully Read Data";
			$result['status_code'] = 202;
		}else{
			$result['error'] = 2;
			$result['message'] = "No data";
			$result['status_code'] = 202;
		}
		$result['total_data'] = count($q);
		$result['data'] = $q;
		return response()->json($result);
	}

	public function list(Request $request)
	{
        $access_detail = $this->CheckAllowAccess($this->kodeMenu, "detail");
        $access_edit = $this->CheckAllowAccess($this->kodeMenu, "edit");
        $access_delete = $this->CheckAllowAccess($this->kodeMenu, "delete");
		$data = myData::select(["id", "name", "email", 'status', 'users_level_id'])->with("users_level");
		$data->where("is_deleted", 0);
		$data->where("users_level_id", "!=", 4);
		return DataTables::eloquent($data)
		->addColumn('opsi', function ($d) use($access_detail, $access_edit, $access_delete) {
			$opsi = '';
			if ($access_detail) {
				$opsi .= "<button class='btn btn-sm btn-success' onclick='Detail(\"" . $d->id . "\")'>Detail</button> ";
			}
			if ($access_edit) {
				$opsi .= "<button class='btn btn-sm btn-info' onclick='Edit(\"" . $d->id . "\")'>Edit</button> ";
			}
			if ($access_delete) {
				$opsi .= "<button class='btn btn-sm btn-danger' onclick='Delete(\"" . $d->id . "\")'>Delete</button> ";
			}
			if ($access_edit) {
				$opsi .= "<button class='btn btn-sm btn-warning' onclick='LoginAs(\"" . $d->id . "\")'>Login As</button> ";
			}
			return $opsi;
		})->editColumn('status', function($d) {
			$badge_bg = ($d->status == "Aktif") ? "badge-primary" : "badge-danger";
			return "<span class='badge $badge_bg'>$d->status</span>";
		})->escapeColumns([])
		->toJson();
	}

	public function add(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'add') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to add";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',
			array(
				'name' => $request->name,
				'email' => $request->email,
				'password' => $request->password,
				'confirm_password' => $request->confirm_password,
				'status' => $request->status,
			)
		);
		$validation_rules = [
            'name' => 'required|max:100',
            'email' => ['required', 'max:200',
				Rule::unique('users')->where(function ($query) use ($request) {
					return $query
						->whereEmail($request->email)
						->whereIsDeleted(0);
				}),
			],
			'password' => 'required|min:5|max:30|',
			'confirm_password' => 'required|min:5|max:30|same:password',
			'status' => 'required',
		];
		if ($request->file('file')) {
			$data_validation['file'] = $request->file;
			$validation_rules['file'] = 'mimes:jpg,png|max:2048';
		}
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Password',
			'confirm_password' => 'Confirm Password',
			'status' => 'Status',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
		}else{
			if (empty($request->users_level_id)) {
				$result['error'] = 2;
				$result['message'] = "Level tidak boleh kosong";
				$this->response($result, 202);exit;
			}
			unset($data_validation['confirm_password']);
            unset($data_validation['file']);
			if ($files = $request->file('file')) {
				$destinationPath = public_path("/uploads/users/foto/");
				$profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
				$files->move($destinationPath, $profileImage);
				$data_validation['file_foto'] = $profileImage;
			}
			$data_validation['created_by'] = Auth::user()->id;
			$data_validation['created_date'] = $this->DateTime();
			$data_validation['password'] = Hash::make($request->password);
			$data_validation['users_level_id'] = $request->users_level_id[0];
			$save = myData::create($data_validation);
			if ($save) {
				if (!empty($request->users_level_id)) {
					foreach ($request->users_level_id as $key => $value) {
						$data_insert = array(
							'users_id' => $save->id,
							'users_level_id' => $value,
							'created_by' => $data_validation['created_by'],
							'created_date' => $data_validation['created_date']
						);
						UsersLevelGroup::create($data_insert);
					}
				}
				$result['error'] = 0;
				$result['message'] = "Successfully Create Data";
			}else{
				$result['error'] = 1;
				$result['message'] = "Error Create Data";
			}
		}
		return response()->json($result);
	}

	public function update(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'edit') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to update";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',array(
			'id' => $request->id,
			'name' => $request->name,
			'email' => $request->email,
			'password' => $request->password,
			'confirm_password' => $request->confirm_password,
			'status' => $request->status,
		));
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
            'email' => ['required', 'max:200',
				Rule::unique('users')->where(function ($query) use ($request) {
					return $query
						->whereEmail($request->email)
						->whereIsDeleted(0)
						->where("id", '!=', $request->id);
				}),
			],
			'status' => 'required',
		];
		if ($request->password) {
			$data['confirm_password'] = 'required|min:5|max:30|same:password';
		}
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'id' => 'Id',
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Password',
			'confirm_password' => 'Confirm Password',
			'status' => 'Status',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			if (empty($request->users_level_id)) {
				$result['error'] = 2;
				$result['message'] = "Level tidak boleh kosong";
				$this->response($result, 202);exit;
			}
			$myData = myData::whereId(e($request->id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				unset($data_validation['password']);
				unset($data_validation['confirm_password']);
				if ($files = $request->file('file')) {
					$destinationPath = public_path("/uploads/users/foto/");
					$profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
					$files->move($destinationPath, $profileImage);
					$data_validation['file_foto'] = $profileImage;
				}
				$data_validation['last_modified_by'] = Auth::user()->id;
				$data_validation['last_modified_date'] = $this->DateTime();
				if ($request->password) {
					$data_validation['password'] = Hash::make($request->password);
				}
				if (!in_array($myData->users_level_id, $request->users_level_id)) {
					$data_validation['users_level_id'] = $request->users_level_id[0];
				}
				if (myData::whereId(e($request->id))->update($data_validation)) {
					if ($request->file('file') and $myData->file_foto != '' and file_exists(public_path('/uploads/users/foto/'. $myData->file_foto)) == true) {
						unlink(public_path('/uploads/users/foto/'.$myData->file_foto));
					}
					if (!empty($request->users_level_id)) {
						UsersLevelGroup::whereNotIn('users_level_id', $request->users_level_id)->delete();
						foreach ($request->users_level_id as $key => $value) {
							$data_insert_users_level_group = array(
								'users_id' => $request->id,
								'users_level_id' => $value,
								'created_by' => $data_validation['last_modified_by'],
								'created_date' => $data_validation['last_modified_date']
							);
							$CheckUsersLevelGroup = UsersLevelGroup::where("users_level_id", $value)->where("users_id", $request->id)->exists();
							if (!$CheckUsersLevelGroup) {
								UsersLevelGroup::create($data_insert_users_level_group);
							}
						}
					}else{
						UsersLevelGroup::where("users_id", $request->id)->delete();
					}
					$result['error'] = 0;
					$result['message'] = "Successfully Update Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Update Data";
				}
			}
		}
		return response()->json($result);
	}

	public function delete(Request $request, $id)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array(
			'id' => e($id),
		);
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				if ($this->IsDeleted($this->table, $id)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Delete Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Delete Data";
				}
			}
		}
		return response()->json($result);
	}

	public function delete_selected(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		foreach ($request->id as $key => $value) {
			$id = e($value);
			$data_validation = array(
				'id' => $id,
			);
			$validation_rules = [
				'id' => 'required|digits_between:1,2|numeric',
			];
			$validator = Validator::make($data_validation, $validation_rules);
			if ($validator->fails()) {
				$result['error_validation'] = $validator->errors();
				$status_validation = true;
			}
			if ($status_validation == true) {
				$result['error'] = 1;
				$result['message'] = "Data is not valid";
			}else{
				$myData = myData::whereId(e($id))->first();
				if ($myData === null) {
					$result['error'] = 2;
					$result['message'] = "Data is not found";
				}else{
					if ($this->IsDeleted($this->table, $id)) {
						$result['error'] = 0;
						$result['message'] = "Successfully Delete Data";
					}else{
						$result['error'] = 1;
						$result['message'] = "Error Delete Data";
						return response()->json($result);exit;
					}
				}
			}	
		}
		return response()->json($result);
	}

	public function switch_level(Request $request, $users_level_id='')
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
		$status_validation = false;
		$id = Auth::user()->id;
		$data_validation = array(
			'id' => e($id),
            'users_level_id' => $users_level_id,
		);
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
			'users_level_id' => 'required|max:2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($id))->where("is_deleted", 0)->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
                $data_update = array(
                    'users_level_id' => $users_level_id,
                );
                if (myData::whereId(e($id))->update($data_update)) {
					$result['error'] = 0;
					$result['message'] = "Berhasil Switch Level";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Switch Level";
				}
			}
		}
		return response()->json($result);
	}

	public function login_as(Request $request, $users_id='')
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
		$status_validation = false;
		$id = Auth::user()->id;
		$data_validation = array(
			'id' => e($id),
			'users_id' => e($users_id),
		);
		$validation_rules = [
			'id' => 'required|numeric',
			'users_id' => 'required|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($users_id))->where("is_deleted", 0)->where("user_level_id", "!=", 4)->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				if (Auth::loginUsingId($users_id, true)) {
					$request->session()->put('is_login_as', 1);
					$request->session()->put('users_id_lama', $id);
					$result['error'] = 0;
					$result['message'] = "Successfully Login As";
				}else{
					$result['error'] = 2;
					$result['message'] = "Error Login As";
				}
			}
		}
		return response()->json($result);
	}
	
}
