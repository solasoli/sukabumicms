<?php

namespace App\Http\Controllers\UsersManagement;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\UsersMenu as myData;
use App\Models\UsersMenu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UsersMenuController extends Controller
{
	var $table = "users_menu";
	var $kodeMenu = "M999003";

	function index()
	{
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
		$file = fopen('./icon.txt', "r");
		$members = array();
		while (!feof($file)) {
		   $members[] = fgets($file);
		}
		fclose($file);
		$listUsersMenu = UsersMenu::select(["*"])->where("rel", 0)->where("is_deleted", 0)
		->orderBy('sequence', 'asc')->get();
		$data = array(
			'firstMenu' => 'M999',
			'secondMenu' => $this->kodeMenu,
			'icon' => $members,
			'listUsersMenu' => $listUsersMenu,
			'access_add' => $this->CheckAllowAccess($this->kodeMenu, "add"),
			'access_edit' => $this->CheckAllowAccess($this->kodeMenu, "edit"),
			'access_detail' => $this->CheckAllowAccess($this->kodeMenu, "detail"),
			'access_delete' => $this->CheckAllowAccess($this->kodeMenu, "delete"),
		);
		return view('users-menu.index', $data);
	}

	public function GetUsersMenu_rel($rel='')
	{	
		return UsersMenu::select(["*"])->where("rel", $rel)
		->orderBy('sequence', 'asc')->get();
	}

	public function where(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Method";
		$result['status_code'] = 203;
		$status_validation = false;
		$validation_rules = [
            'id' => 'min:1|max:20|numeric',
			'limit' => 'max:1|numeric',
			'count' => 'max:1|numeric',
		];
		$validator = Validator::make($request->all(), $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
			return response()->json($result);exit;
		}
		$q = myData::select(["*"])->with(["created_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["last_modified_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["parent_menu" => function($query){
			$query->select(["id", "kode_menu", "name_menu"]);
		}]);
		if ($request->id) {
			$q->where("id", $request->id);
		}
		$q = $q->get();
		if (count($q) > 0) {
			$result['error'] = 0;
			$result['message'] = "Successfully Read Data";
			$result['status_code'] = 202;
		}else{
			$result['error'] = 2;
			$result['message'] = "No data";
			$result['status_code'] = 202;
		}
		$result['total_data'] = count($q);
		$result['data'] = $q;
		return response()->json($result);
	}

	public function add(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'add') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to add";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',
			array(
				'kode_menu' => $request->kode_menu,
				'name_menu' => $request->name_menu,
				'url' => $request->url,
				'rel' => $request->rel,
				'sequence' => $request->sequence,
				'icon' => $request->icon,
			)
		);
		$validation_rules = [
            'kode_menu' => ['required', 'max:50',
				Rule::unique('users_menu')->where(function ($query) use ($request) {
					return $query
						->whereKodeMenu($request->kode_menu)
						->whereIsDeleted(0);
				}),
			],
			'name_menu' => 'required|max:100',
			'url' => 'required|max:200',
			'sequence' => 'required|max:1000|numeric',
			'rel' => 'required|digits_between:1,2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'kode_menu' => 'Kode Menu',
			'name_menu' => 'Nama Menu',
			'url' => 'Url',
			'icon' => 'Icon',
			'sequence' => 'Sequence',
			'rel' => 'Rel',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
		}else{
			$data_validation['created_by'] = Auth::user()->id;
			$data_validation['created_date'] = $this->DateTime();
			if (myData::create($data_validation)) {
				$result['error'] = 0;
				$result['message'] = "Successfully Create Data";
			}else{
				$result['error'] = 1;
				$result['message'] = "Error Create Data";
			}
		}
		if ($result['error'] == 0) {
			Session::flash('success', $result['message']);
		}
		return response()->json($result);
	}

	public function update(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'edit') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to update";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',array(
			'id' => $request->id,
            'kode_menu' => $request->kode_menu,
            'name_menu' => $request->name_menu,
            'url' => $request->url,
            'rel' => $request->rel,
            'sequence' => $request->sequence,
            'icon' => $request->icon,
		));
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
            'kode_menu' => ['required', 'max:50',
				Rule::unique('users_menu')->where(function ($query) use ($request) {
					return $query
						->whereKodeMenu($request->kode_menu)
						->whereIsDeleted(0)
						->where("id", '!=', $request->id);
				}),
			],
			'name_menu' => 'required|max:100',
			'url' => 'required|max:200',
			'icon' => 'max:100',
			'sequence' => 'required|max:1000|numeric',
			'rel' => 'required|digits_between:1,2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'id' => 'Id',
			'kode_menu' => 'Kode Menu',
			'name_menu' => 'Nama Menu',
			'url' => 'Url',
			'icon' => 'Icon',
			'sequence' => 'Sequence',
			'rel' => 'Rel',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($request->id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				$data_validation['last_modified_by'] = Auth::user()->id;
				$data_validation['last_modified_date'] = $this->DateTime();
				if (myData::whereId(e($request->id))->update($data_validation)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Update Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Update Data";
				}
			}
		}
		if ($result['error'] == 0) {
			Session::flash('success', $result['message']);
		}
		return response()->json($result);
	}

	public function delete(Request $request, $id)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array(
			'id' => e($id),
		);
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				if ($this->IsDeleted($this->table, $id)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Delete Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Delete Data";
				}
			}
		}
		if ($result['error'] == 0) {
			Session::flash('success', $result['message']);
		}
		return response()->json($result);
	}

	public function delete_selected(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		foreach ($request->id as $key => $value) {
			$id = e($value);
			$data_validation = array(
				'id' => $id,
			);
			$validation_rules = [
				'id' => 'required|digits_between:1,2|numeric',
			];
			$validator = Validator::make($data_validation, $validation_rules);
			if ($validator->fails()) {
				$result['error_validation'] = $validator->errors();
				$status_validation = true;
			}
			if ($status_validation == true) {
				$result['error'] = 1;
				$result['message'] = "Data is not valid";
			}else{
				$myData = myData::whereId(e($id))->first();
				if ($myData === null) {
					$result['error'] = 2;
					$result['message'] = "Data is not found";
				}else{
					if ($this->IsDeleted($this->table, $id)) {
						$result['error'] = 0;
						$result['message'] = "Successfully Delete Data";
					}else{
						$result['error'] = 1;
						$result['message'] = "Error Delete Data";
						return response()->json($result);exit;
					}
				}
			}	
		}
		if ($result['error'] == 0) {
			Session::flash('success', $result['message']);
		}
		return response()->json($result);
	}


    public function GetMenuAccess($rel='')
    {
        $q = DB::table("users_menu as um")
		->select(["um.*", "ua.access"])
		->join('users_access as ua', 'ua.users_menu_id', '=', 'um.id')
		->where("um.is_deleted", 0)
		->where("users_level_id", Auth::user()->users_level_id)
		->orderBy("um.sequence", "asc")
		->where("access", "show");
		if ($rel) {
			$q->where("um.rel", $rel);
		}else{
			$q->where("um.rel", 0);
		}
		return $q->get();
    }
    
}
