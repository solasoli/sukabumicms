<?php

namespace App\Http\Controllers\UsersManagement;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\UsersLevelGroup as myData;

class UsersLevelGroupController extends Controller
{

	public function where(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Method";
		$result['status_code'] = 203;
		$status_validation = false;
		$validation_rules = [
            'id' => 'min:1|max:20|numeric',
            'users_id' => 'min:1|max:20|numeric',
			'limit' => 'max:1|numeric',
			'count' => 'max:1|numeric',
		];
		$validator = Validator::make($request->all(), $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
			return response()->json($result);exit;
		}
		$q = myData::select(["*"])->with(["created_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["last_modified_by_user" => function($query){
			$query->select(["id", "name"]);
		}])->with(["users" => function($query){
			$query->select(["id", "name"]);
		}])->with("users_level");
		if ($request->id) {
			$q->where("id", $request->id);
		}
        if ($request->users_id) {
            $q->where("users_id", $request->users_id);
        }
		$q = $q->get();
		if (count($q) > 0) {
			$result['error'] = 0;
			$result['message'] = "Successfully Read Data";
			$result['status_code'] = 202;
		}else{
			$result['error'] = 2;
			$result['message'] = "No data";
			$result['status_code'] = 202;
		}
		$result['total_data'] = count($q);
		$result['data'] = $q;
		return response()->json($result);
	}

}
