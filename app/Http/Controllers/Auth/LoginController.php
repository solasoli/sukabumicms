<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {		
        return view('auth.login');
    }

    public function proses_login(Request $request)
    {
        request()->validate([
        'email' => 'required:|max:150',
        'password' => 'required|max:30',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('/home');
        }
        return redirect('login')->withSuccess('Oppes! Silahkan Cek Inputanmu');
    }

    public function logout(Request $request) {
        if (Session::get("is_login_as")) {
            Auth::loginUsingId(Session::get('users_id_lama'), true);
            Session::forget("is_login_as");
            Session::forget("users_id_lama");
            return Redirect('home');
        }else{
            $request->session()->flush();
            Auth::logout();
            return Redirect('login');
        }
    }
}
