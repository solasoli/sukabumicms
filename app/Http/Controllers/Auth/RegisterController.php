<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\RefElementMasyarakat;
use App\Models\RefPerangkatDaerah;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Users;
use App\Models\UsersLevelGroup;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $data = array(
            'listPerangkatDaerah' => RefPerangkatDaerah::select(["id", "nama_perangkat_daerah"])->where('is_deleted', 0)->get(),
            'listElementMasyarakat' => RefElementMasyarakat::select(["id", "nama_element_masyarakat"])->where("is_deleted", 0)->get(),
        );
		return view('auth.register', $data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function add(Request $request)
    {
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
		$status_validation = false;
		$data_validation = array_map('e',
			array(
				'name' => $request->name,
				'email' => $request->email,
                'no_telp' => $request->no_telp,
				'password' => $request->password,
				'password_confirmation' => $request->password_confirmation,
				'instansi' => $request->instansi,
				'perangkat_daerah_id' => $request->perangkat_daerah_id,
				'element_masyarakat_id' => $request->element_masyarakat_id,
			)
		);
		$validation_rules = [
            'name' => ['required', 'max:200'],
            'email' => ['required', 'max:200',
				Rule::unique('users')->where(function ($query) use ($request) {
					return $query
						->whereEmail($request->email)
						->whereIsDeleted(0);
				}),
			],
			'no_telp' => 'required|max:50',
			'password' => 'required|min:5|max:30|',
			'password_confirmation' => 'required|min:5|max:30|same:password',
            'element_masyarakat_id' => 'required|numeric',
		];
        if ($request->lainnya == 1) {
            unset($data_validation['perangkat_daerah_id']);
            $validation_rules['instansi'] = "required|max:250";
        }else{
            unset($data_validation['instansi']);
            $validation_rules['perangkat_daerah_id'] = "required|numeric";
        }
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'name' => 'Nama',
			'email' => 'Email',
			'no_telp' => 'No. Telp',
			'password' => 'Kata Sandi',
			'password_confirmation' => 'Konfirmasi Kata Sandi',
            'instansi' => 'Instansi',
            'element_masyarakat' => 'Element Masyarakat',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
		}else{
            unset($data_validation['password_confirmation']);
			$data_validation['created_date'] = $this->DateTime();
            $data_validation['users_level_id'] = 4;
			$data_validation['password'] = Hash::make($request->password);
            $data_validation['is_konfirmasi'] = 0;
            $save = Users::create($data_validation);
			if ($save) {
                $data_insert = array(
                    'users_id' => $save->id,
                    'users_level_id' => $data_validation['users_level_id'],
                    'created_by' => $save->id,
                    'created_date' => $data_validation['created_date'],
                );
                UsersLevelGroup::create($data_insert);
				$result['error'] = 0;
				$result['message'] = "Berhasil Mendaftar";
			}else{
				$result['error'] = 1;
				$result['message'] = "Error Register";
			}
		}
		return response()->json($result);
    }
}
