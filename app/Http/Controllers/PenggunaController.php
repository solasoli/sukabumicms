<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\RefElementMasyarakat;
use App\Models\RefPerangkatDaerah;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Users as myData;
use App\Models\UsersLevelGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{	
	var $table = "users";
	var $kodeMenu = "M998001";

	function index()
	{
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
		$data = array(
			'firstMenu' => 'M998',
			'secondMenu' => $this->kodeMenu,
			'access_add' => $this->CheckAllowAccess($this->kodeMenu, "add"),
			'access_edit' => $this->CheckAllowAccess($this->kodeMenu, "edit"),
			'access_delete' => $this->CheckAllowAccess($this->kodeMenu, "delete"),
            'listElementMasyarakat' => RefElementMasyarakat::where("is_deleted", 0)->get(),
            'listPerangkatDaerah' => RefPerangkatDaerah::where("is_deleted", 0)->get(),
		);
		return view('pengguna.index', $data);
	}

	public function list(Request $request)
	{
        $access_detail = $this->CheckAllowAccess($this->kodeMenu, "detail");
        $access_edit = $this->CheckAllowAccess($this->kodeMenu, "edit");
        $access_delete = $this->CheckAllowAccess($this->kodeMenu, "delete");
		$data = myData::select(["users.id", "name", "email", 'status', 'instansi', 'element_masyarakat_id', 'perangkat_daerah_id'])->with('element_masyarakat')->with("perangkat_daerah");
		$data->where("is_deleted", 0);
		$data->where("users_level_id", 4);
		$data->where("is_konfirmasi", 1);
		return DataTables::eloquent($data)
		->addColumn('opsi', function ($d) use($access_detail, $access_edit, $access_delete) {
			$opsi = '';
			if ($access_detail) {
				$opsi .= "<button class='btn btn-sm btn-success' onclick='Detail(\"" . $d->id . "\")'>Detail</button> ";
			}
			if ($access_edit) {
				$opsi .= "<button class='btn btn-sm btn-info' onclick='Edit(\"" . $d->id . "\")'>Edit</button> ";
			}
			if ($access_delete) {
				$opsi .= "<button class='btn btn-sm btn-danger' onclick='Delete(\"" . $d->id . "\")'>Delete</button> ";
			}
			if ($access_edit) {
				$opsi .= "<button class='btn btn-sm btn-warning' onclick='LoginAs(\"" . $d->id . "\")'>Login As</button> ";
			}
			return $opsi;
		})->editColumn('status', function($d) {
			$badge_bg = ($d->status == "Aktif") ? "badge-primary" : "badge-danger";
			return "<span class='badge $badge_bg'>$d->status</span>";
		})->editColumn('instansi', function($d) {
			return ($d->instansi) ? $d->instansi : $d->perangkat_daerah->nama_perangkat_daerah;
		})->escapeColumns([])
		->toJson();
	}

	public function add(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'add') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to add";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',
			array(
				'name' => $request->name,
				'email' => $request->email,
                'no_telp' => $request->no_telp,
				'password' => $request->password,
				'password_confirmation' => $request->password_confirmation,
				'instansi' => $request->instansi,
				'perangkat_daerah_id' => $request->perangkat_daerah_id,
				'element_masyarakat_id' => $request->element_masyarakat_id,
                'status' => $request->status,
			)
		);
		$validation_rules = [
            'name' => ['required', 'max:200'],
            'email' => ['required', 'max:200',
				Rule::unique('users')->where(function ($query) use ($request) {
					return $query
						->whereEmail($request->email)
						->whereIsDeleted(0);
				}),
			],
			'no_telp' => 'required|max:50',
			'password' => 'required|min:5|max:30',
			'password_confirmation' => 'required|min:5|max:30|same:password',
            'element_masyarakat_id' => 'required|numeric',
            'status' => 'required',
		];
        if ($request->lainnya == 1) {
            $data_validation['perangkat_daerah_id'] = null;
            $validation_rules['instansi'] = "required|max:250";
        }else{
            $data_validation['instansi'] = null;
            $validation_rules['perangkat_daerah_id'] = "required|numeric";
        }
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'name' => 'Nama Penanggung Jawab',
			'email' => 'Email',
			'no_telp' => 'No. Telp',
			'password' => 'Kata Sandi',
			'password_confirmation' => 'Konfirmasi Kata Sandi',
            'instansi' => 'Instansi',
            'element_masyarakat' => 'Jenis Pengguna',
            'status' => 'Status',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not found";
		}else{
			unset($data_validation['password_confirmation']);
			$data_validation['created_by'] = Auth::user()->id;
			$data_validation['created_date'] = $this->DateTime();
			$data_validation['password'] = Hash::make($request->password);
			$data_validation['users_level_id'] = 4;
			$data_validation['is_konfirmasi'] = 1;
			$save = myData::create($data_validation);
			if ($save) {
                $data_insert = array(
                    'users_id' => $save->id,
                    'users_level_id' => 4,
                    'created_by' => $data_validation['created_by'],
                    'created_date' => $data_validation['created_date']
                );
                UsersLevelGroup::create($data_insert);
				$result['error'] = 0;
				$result['message'] = "Successfully Create Data";
			}else{
				$result['error'] = 1;
				$result['message'] = "Error Create Data";
			}
		}
		return response()->json($result);
	}

	public function update(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'edit') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to update";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array_map('e',array(
			'id' => $request->id,
            'name' => $request->name,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
            'instansi' => $request->instansi,
            'perangkat_daerah_id' => $request->perangkat_daerah_id,
            'element_masyarakat_id' => $request->element_masyarakat_id,
            'status' => $request->status,
		));
		$validation_rules = [
			'id' => 'required|digits_between:1,20|numeric',
            'email' => ['required', 'max:200',
				Rule::unique('users')->where(function ($query) use ($request) {
					return $query
						->whereEmail($request->email)
						->whereIsDeleted(0)
						->where("id", '!=', $request->id);
				}),
			],
			'no_telp' => 'required|max:50',
			'password' => 'min:5|max:30',
            'element_masyarakat_id' => 'required|numeric',
            'status' => 'required',
		];
		if ($request->password) {
			$data['password_confirmaion'] = 'required|min:5|max:30|same:password';
		}
        if ($request->lainnya == 1) {
            $data_validation['perangkat_daerah_id'] = null;
            $validation_rules['instansi'] = "required|max:250";
        }else{
            $data_validation['instansi'] = null;
            $validation_rules['perangkat_daerah_id'] = "required|numeric";
        }
		$validator = Validator::make($data_validation, $validation_rules);
		$validator->setAttributeNames([
			'id' => 'Id',
			'name' => 'Nama Penanggung Jawab',
			'email' => 'Email',
			'no_telp' => 'No. Telp',
			'password' => 'Kata Sandi',
			'password_confirmation' => 'Konfirmasi Kata Sandi',
            'instansi' => 'Instansi',
            'element_masyarakat' => 'Jenis Pengguna',
            'status' => 'Status',
		]);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($request->id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				unset($data_validation['password']);
				unset($data_validation['password_confirmation']);
				$data_validation['last_modified_by'] = Auth::user()->id;
				$data_validation['last_modified_date'] = $this->DateTime();
				if ($request->password) {
					$data_validation['password'] = Hash::make($request->password);
				}
				if (myData::whereId(e($request->id))->update($data_validation)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Update Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Update Data";
				}
			}
		}
		return response()->json($result);
	}

	public function delete(Request $request, $id)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array(
			'id' => e($id),
		);
		$validation_rules = [
			'id' => 'required|digits_between:1,20|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($id))->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				if ($this->IsDeleted($this->table, $id)) {
					$result['error'] = 0;
					$result['message'] = "Successfully Delete Data";
				}else{
					$result['error'] = 1;
					$result['message'] = "Error Delete Data";
				}
			}
		}
		return response()->json($result);
	}

	public function delete_selected(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		foreach ($request->id as $key => $value) {
			$id = e($value);
			$data_validation = array(
				'id' => $id,
			);
			$validation_rules = [
				'id' => 'required|digits_between:1,20|numeric',
			];
			$validator = Validator::make($data_validation, $validation_rules);
			if ($validator->fails()) {
				$result['error_validation'] = $validator->errors();
				$status_validation = true;
			}
			if ($status_validation == true) {
				$result['error'] = 1;
				$result['message'] = "Data is not valid";
			}else{
				$myData = myData::whereId(e($id))->first();
				if ($myData === null) {
					$result['error'] = 2;
					$result['message'] = "Data is not found";
				}else{
					if ($this->IsDeleted($this->table, $id)) {
						$result['error'] = 0;
						$result['message'] = "Successfully Delete Data";
					}else{
						$result['error'] = 1;
						$result['message'] = "Error Delete Data";
						return response()->json($result);exit;
					}
				}
			}	
		}
		return response()->json($result);
	}

	public function login_as(Request $request, $users_id='')
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
		$status_validation = false;
		$id = Auth::user()->id;
		$data_validation = array(
			'id' => e($id),
			'users_id' => e($users_id),
		);
		$validation_rules = [
			'id' => 'required|numeric',
			'users_id' => 'required|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($users_id))->where("is_deleted", 0)->where("users_level_id", 4)->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
				if (Auth::loginUsingId($users_id, true)) {
					$request->session()->put('is_login_as', 1);
					$request->session()->put('users_id_lama', $id);
					$result['error'] = 0;
					$result['message'] = "Successfully Login As";
				}else{
					$result['error'] = 2;
					$result['message'] = "Error Login As";
				}
			}
		}
		return response()->json($result);
	}
}
