<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use App\Models\Users as myData;
use Illuminate\Support\Facades\Auth;

class PenggunaTertolakController extends Controller
{	
	var $table = "users";
	var $kodeMenu = "M998003";

	function index()
	{
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
		$data = array(
			'firstMenu' => 'M998',
			'secondMenu' => $this->kodeMenu,
			'access_add' => $this->CheckAllowAccess($this->kodeMenu, "add"),
			'access_edit' => $this->CheckAllowAccess($this->kodeMenu, "edit"),
			'access_delete' => $this->CheckAllowAccess($this->kodeMenu, "delete"),
		);
		return view('pengguna.tertolak', $data);
	}

	public function list(Request $request)
	{
        $access_detail = $this->CheckAllowAccess($this->kodeMenu, "detail");
        $access_edit = $this->CheckAllowAccess($this->kodeMenu, "edit");
        $access_delete = $this->CheckAllowAccess($this->kodeMenu, "delete");
		$data = myData::select(["users.id", "name", "email", 'status', 'instansi', 'element_masyarakat_id', 'perangkat_daerah_id'])->with('element_masyarakat')->with("perangkat_daerah");
		$data->where("is_deleted", 0);
		$data->where("users_level_id", 4);
		$data->where("is_konfirmasi", 2);
		return DataTables::eloquent($data)
		->addColumn('opsi', function ($d) use($access_detail, $access_edit, $access_delete) {
			$opsi = '';
			if ($access_detail) {
				$opsi .= "<button class='btn btn-sm btn-success' onclick='Detail(\"" . $d->id . "\")'>Detail</button> ";
			}
			if ($access_edit) {
                $opsi .= "
                <div class='btn-group btn-sm' role='group' aria-label='Basic example' style='padding:0px;'>
                    <button type='button' class='btn btn-primary btn-sm' onclick='Konfirmasi(\"" . $d->id . "\", 1)'>Back Konfirmasi</button>
                    <button type='button' class='btn btn-danger btn-sm' onclick='Konfirmasi(\"" . $d->id . "\", 2)'>Delete</button>
                </div>";
			}
			return $opsi;
		})->editColumn('status', function($d) {
			$badge_bg = ($d->status == "Aktif") ? "badge-primary" : "badge-danger";
			return "<span class='badge $badge_bg'>$d->status</span>";
		})->editColumn('instansi', function($d) {
			return ($d->instansi) ? $d->instansi : $d->perangkat_daerah->nama_perangkat_daerah;
		})->escapeColumns([])
		->toJson();
	}

	public function konfirmasi(Request $request, $id='', $is_konfirmasi='')
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'edit') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		$data_validation = array(
			'id' => e($id),
            'is_konfirmasi' => $is_konfirmasi,
		);
		$validation_rules = [
			'id' => 'required|digits_between:1,2|numeric',
			'is_konfirmasi' => 'required|max:2|numeric',
		];
		$validator = Validator::make($data_validation, $validation_rules);
		if ($validator->fails()) {
			$result['error_validation'] = $validator->errors();
			$status_validation = true;
		}
		if ($status_validation == true) {
			$result['error'] = 1;
			$result['message'] = "Data is not valid";
		}else{
			$myData = myData::whereId(e($id))->where("is_konfirmasi", "!=", 0)->first();
			if ($myData === null) {
				$result['error'] = 2;
				$result['message'] = "Data is not found";
			}else{
                if ($is_konfirmasi == 1) {
                    $data_update = array(
                        'is_konfirmasi' => 0,
                        'is_konfirmasi_by' => Auth::user()->id,
                        'is_konfirmasi_date' => $this->DateTime(),
                    );
                    if (myData::whereId(e($request->id))->update($data_update)) {
                        $result['error'] = 0;
                        $result['message'] = "Berhasil kembali ke Menunggu Konfirmasi";
                    }else{
                        $result['error'] = 1;
                        $result['message'] = "Error Set Konfirmasi Data";
                    }
                }else{
                    if ($this->IsDeleted($this->table, $id)) {
                        $result['error'] = 0;
                        $result['message'] = "Successfully Delete Data";
                    }else{
                        $result['error'] = 1;
                        $result['message'] = "Error Delete Data";
                    }
                }
			}
		}
		return response()->json($result);
	}

	public function konfirmasi_selected(Request $request)
	{
		$result['error'] = 3;
		$result['message'] = "Error Requests";
		$result['status_code'] = 203;
        if ($this->CheckAllowAccess($this->kodeMenu, 'delete') == null){
			$result['error'] = 6;
			$result['message'] = "Not allowed to delete";
			$result['status_code'] = 201;
			return response()->json($result);exit;
        }
		$status_validation = false;
		foreach ($request->id as $key => $value) {
			$id = e($value);
			$data_validation = array(
				'id' => $id,
                'is_konfirmasi' => $request->is_konfirmasi,
			);
			$validation_rules = [
				'id' => 'required|digits_between:1,2|numeric',
                'is_konfirmasi' => 'required|max:2|numeric',
			];
			$validator = Validator::make($data_validation, $validation_rules);
			if ($validator->fails()) {
				$result['error_validation'] = $validator->errors();
				$status_validation = true;
			}
			if ($status_validation == true) {
				$result['error'] = 1;
				$result['message'] = "Data is not valid";
			}else{
				$myData = myData::whereId(e($id))->where('is_deleted', 0)->first();
				if ($myData === null) {
					$result['error'] = 2;
					$result['message'] = "Data is not found";
				}else{
                    if ($request->is_konfirmasi == 1) {
                        $data_update = array(
                            'is_konfirmasi' => 0,
                            'is_konfirmasi_by' => Auth::user()->id,
                            'is_konfirmasi_date' => $this->DateTime(),
                        );
                        if (myData::whereId(e($id))->update($data_update)) {
                            $result['error'] = 0;
                            $result['message'] = "Berhasil kembali ke Menunggu Konfirmasi";
                        }else{
                            $result['error'] = 1;
                            $result['message'] = "Error Set Konfirmasi Data";
                        }
                    }else{
                        if ($this->IsDeleted($this->table, $id)) {
                            $result['error'] = 0;
                            $result['message'] = "Successfully Delete Data";
                        }else{
                            $result['error'] = 1;
                            $result['message'] = "Error Delete Data";
                        }
                    }
				}
			}	
		}
		return response()->json($result);
	}
}
