<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    var $kodeMenu = "M001";
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if ($this->CheckAllowAccess($this->kodeMenu, 'show') == null){
            abort(404);
        }
        $data = array(
            'firstMenu' => $this->kodeMenu,
            'secondMenu' => ''
        );
        return view('dashboard', $data);
    }
}
