<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersMenu extends Model
{
    use HasFactory;
	protected $table = "users_menu";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];

	public function created_by_user()
	{
		return $this->belongsTo(User::class, 'created_by')->select(['id', 'name']);
	}

	public function last_modified_by_user()
	{
		return $this->belongsTo(User::class, 'last_modified_by')->select(['id', 'name']);
	}

    public function parent_menu()
    {
		return $this->belongsTo(UsersMenu::class, 'rel')->select(['id', 'kode_menu', 'name_menu']);
    }
}
