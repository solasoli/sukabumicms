<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersAccess extends Model
{
    use HasFactory;
	protected $table = "users_access";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];

	public function created_by_user()
	{
		return $this->belongsTo(Users::class, 'created_by')->select(['id', 'name']);
	}

	public function users_level()
	{
		return $this->belongsTo(UsersLevel::class, 'users_level_id')->select(['id', 'name_level']);
	}

	public function users_menu()
	{
		return $this->belongsTo(UsersMenu::class, 'users_menu_id')->select(['id', 'kode_menu', 'name_menu', 'url', 'rel']);
	}

}
