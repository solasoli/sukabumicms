<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inovasi extends Model
{
    use HasFactory;
	protected $table = "inovasi";
	protected $primaryKey = "id";
	public $timestamps = false;
    protected $guarded = [];

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id')->select(['id', 'name']);
	}

	public function created_by_user()
	{
		return $this->belongsTo(User::class, 'created_by')->select(['id', 'name']);
	}

	public function last_modified_by_user()
	{
		return $this->belongsTo(User::class, 'last_modified_by')->select(['id', 'name']);
	}

	public function klasifikasi_kategori()
	{
		return $this->belongsTo(RefKlasifikasiKategori::class, 'klasifikasi_kategori_id')->select(['id', 'nama_klasifikasi_kategori']);
	}
}
