<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="#">{{ $titlePage }}</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
                {{-- <div class="input-group no-border">
                    <input type="text" value="" class="form-control" placeholder="Search...">
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                    </button>
                </div> --}}
            </form>
            <ul class="navbar-nav">
                <?php
                $auth = \App\Http\Controllers\Controller::Auth();
                $authUsersLevel = \App\Http\Controllers\Controller::AuthUsersLevel();
                ?>
                @if (!$authUsersLevel->get())
                    <li class="nav-item dropdown">
                        <a class="nav-link btn btn-success text-white" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{-- <i class="material-icons">notifications</i> --}}
                            {{$auth->users_level->name_level}}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            @foreach ($authUsersLevel->get() as $value)
                            @if ($value->users_level_id != $auth->users_level_id)
                            <a class="dropdown-item" href="javascript:void(0)" onclick="SwitchLevel('{{$value->users_level_id}}')">{{ $value->users_level->name_level }}</a>
                            @endif
                            @endforeach
                        </div>
                    </li>
                @else   
                    <li class="nav-item">
                        <a class="nav-link btn-primary text-white" href="#" style="cursor: inherit">
                            {{$auth->users_level->name_level}}
                        </a>
                    </li>
                @endif
                @if (Session::get("is_login_as"))
                    <li class="nav-item">
                        <a class="nav-link btn-danger text-white" href="#" style="cursor: inherit">
                            Login As Mode
                        </a>
                    </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="Logout()">
                        <i class="material-icons">person</i>
                        Logout
                    </a>
                </li>
                {{-- <li class="nav-item dropdown">
          <a class="nav-link" href="{{route('logout')}}" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">person</i>
                <p class="d-lg-none d-md-block">
                    {{ __('Account') }}
                </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                    <a class="dropdown-item" href="{{ route('profile.edit') }}">{{ __('Profile') }}</a>
                    <a class="dropdown-item" href="#">{{ __('Settings') }}</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Log out') }}</a>
                </div>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>