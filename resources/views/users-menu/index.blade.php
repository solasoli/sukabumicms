@extends('layouts.app', [
    'firstMenu' => $firstMenu,
    'secondMenu' => $secondMenu,
    'titlePage' => __('Users Menu')])

@section('content')
<style>
  #DataTable tbody tr td:nth-child(1),#DataTable tbody tr td:nth-child(2),#DataTable tbody tr td:nth-child(3),#DataTable tbody tr td:nth-child(8) {
      text-align: center;
  }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Data Users Menu</h4>
                        {{-- <p class="card-category"> Here is a subtitle for this table</p> --}}
                    </div>
                    <div class="card-body">

                        @if ($access_add)
                            <button type="button" class="btn btn-sm btn-primary" onclick="Add()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Add Data" data-original-title="">Add</button>
                        @endif

                        @if ($access_delete)
                            <button type="button" id="DeleteSelected" class="btn btn-sm btn-danger" onclick="DeleteSelected()" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="Delete Selected" style="display: none;">Delete Selected</button>
                        @endif

                        <table class="table table-striped table-hover DataTable" id="DataTable" style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="5%" style="vertical-align: middle;">No.</th>
                                    <th width="5%" class="text-center">
                                        <input type="checkbox" id="CheckAll" style="position: relative;left: 0px;opacity: 1;">
                                    </th>
                                    <th width="5%" class="text-center">Opsi</th>
                                    <th>Kode Menu</th>
                                    <th>Name Menu</th>
                                    <th>Url</th>
                                    <th width="5%">Icon</th>
                                    <th width="5%">Sequence</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=0;?>
                                @foreach ($listUsersMenu as $key => $value)
                                <?php $no++;?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>
                                            <input type='checkbox' id='CheckboxRow' onclick='CheckTotalCheckedDelete()' data-id='{{$value->id}}' style='position: relative;left: 0px;opacity: 1;'/>
                                        </td>
                                        <td>
                                            @if ($access_detail)
                                                <button class='btn btn-sm btn-success' onclick='Detail("{{$value->id}}")'>Detail</button>
                                            @endif
                                            @if ($access_edit)
                                                <button class='btn btn-sm btn-info' onclick='Edit("{{$value->id}}")'>Edit</button>
                                            @endif
                                            @if ($access_delete)
                                                <button class='btn btn-sm btn-danger' onclick='Delete("{{$value->id}}")'>Delete</button>
                                            @endif
                                        </td>
                                        <td>{{$value->kode_menu}}</td>
                                        <td class="text-left">{{$value->name_menu}}</td>
                                        <td>{{$value->url}}</td>
                                        <td>
                                            <i class="material-icons">{{ $value->icon}}</i>
                                        </td>
                                        <td>{{$value->sequence}}</td>
                                    </tr>
                                    <?php
                                        $GetUsersMenu_rel = \App\Http\Controllers\UsersManagement\UsersMenuController::GetUsersMenu_rel($value->id);
                                    ?>
                                    @foreach ($GetUsersMenu_rel as $k => $v)
                                    <?php $no++;?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>
                                            <input type='checkbox' id='CheckboxRow' onclick='CheckTotalCheckedDelete()' data-id='{{$v->id}}' style='position: relative;left: 0px;opacity: 1;'/>
                                        </td>
                                        <td>
                                            @if ($access_detail)
                                                <button class='btn btn-sm btn-success' onclick='Detail("{{$v->id}}")'>Detail</button>
                                            @endif
                                            @if ($access_edit)
                                                <button class='btn btn-sm btn-info' onclick='Edit("{{$v->id}}")'>Edit</button>
                                            @endif
                                            @if ($access_delete)
                                                <button class='btn btn-sm btn-danger' onclick='Delete("{{$v->id}}")'>Delete</button>
                                            @endif
                                        </td>
                                        <td>{{$v->kode_menu}}</td>
                                        <td class="text-left">{{$v->name_menu}}</td>
                                        <td>{{$v->url}}</td>
                                        <td>
                                            <i class="material-icons">{{ $v->icon}}</i>
                                        </td>
                                        <td>{{$v->sequence}}</td>
                                    </tr>
                                    @endforeach
                                    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="form">
                @csrf
                <input type="hidden" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kode Menu</label>
                        <div class="form-line">
                            <input type="text" class="form-control" name="kode_menu" required maxlength="50">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Name Menu</label>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name_menu" required maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Url</label>
                        <div class="form-line">
                            <input type="text" class="form-control" name="url" required maxlength="200">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Urutan</label>
                        <div class="form-line">
                            <input type="number" class="form-control" name="sequence" required maxlength="11">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Parent</label>
                        <div class="form-line">
                            <select class="form-control" name="rel" required>
                                <option value="0">Pilih</option>
                                @foreach ($listUsersMenu as $key => $value)
                                    <option value="{{$value->id}}">{{$value->kode_menu." - ".$value->name_menu}}</option>
                                    <?php
                                        $GetUsersMenu_rel = \App\Http\Controllers\UsersManagement\UsersMenuController::GetUsersMenu_rel($value->id);
                                    ?>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table-icon">
                                <tr>
                                    @foreach ($icon as $k => $v)
                                    <th class="text-center">
                                            <i class="material-icons">{{ str_replace('"', '', $v)}}</i><br>
                                            <input type="radio" name="icon" value="{{$v}}">
                                        </th>
                                    @endforeach
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table table-detail table-striped">
                    <tr>
                        <th width="5%">Kode menu</th>
                        <th width="1%">:</th>
                        <td class="kode_menu"></td>
                    </tr>
                    <tr>
                        <th>Name Menu</th>
                        <th>:</th>
                        <td class="name_menu"></td>
                    </tr>
                    <tr>
                        <th>Url</th>
                        <th>:</th>
                        <td class="url"></td>
                    </tr>
                    <tr>
                        <th>Sequence</th>
                        <th>:</th>
                        <td class="sequence"></td>
                    </tr>
                    <tr>
                        <th>Icon</th>
                        <th>:</th>
                        <td class="icon"></td>
                    </tr>
                    <tr>
                        <th>Parent</th>
                        <th>:</th>
                        <td class="parent_menu_name"></td>
                    </tr>
                    <tr>
                        <th>Created By</th>
                        <th>:</th>
                        <td class="created_by_name"></td>
                    </tr>
                    <tr>
                        <th>Created Date</th>
                        <th>:</th>
                        <td class="created_date"></td>
                    </tr>
                    <tr>
                        <th>Last Modified By</th>
                        <th>:</th>
                        <td class="last_modified_by_name"></td>
                    </tr>
                    <tr>
                        <th>Last Modified Date</th>
                        <th>:</th>
                        <td class="last_modified_date"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var table = "";
    var module_api = "<?php echo url('/users-menu'); ?>";
    var save_type = "Add";
    $(document).ready(function() {
        @if( Session::has("success") )
            toastr.info("{{ Session::get("success") }}");
        @endif
        @if( Session::has("error") )
            toastr.error("{{ Session::get("error") }}");
        @endif
        $('#form').on("submit", function(event) {
            event.preventDefault();
            var url;
            if (save_type == "Add") {
                var url = ""
            } else {
                var url = "/update"
            }
            ResetValidation();
            Swal.fire({
                title: 'Are you sure?',
                text: "Save Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {
                    setTimeout(() => {
                        $.ajax({
                            url: module_api + url,
                            data: new FormData($('#form')[0]),
                            type: 'post',
                            contentType: false,
                            processData: false,
                            async: false,
                            dataType: 'json',
                            success: function(data) {
                                if (data.error == 0) {
                                    location.reload();
                                } else if (data.error == 1) {
                                    setTimeout(() => {
                                        $.each(data.error_validation, function(key, value) {
                                            ValidationPopover('[name=' + key + ']', value);
                                        });
                                    }, 0);
                                    toastr.warning(data.message);
                                } else {
                                    toastr.warning(data.message);
                                }
                                swal.close();
                            },
                            error: function(data) {
                                swal.close();
                                toastr.error("Error " + save_type + " Data");
                            }
                        });
                    }, 150);
                }
            })
        });
        table = $('#DataTable').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, 200, 350, -1],
                [10, 25, 50, 100, 200, 350, "All"]
            ],
            "language": {
                "emptyTable": "<center>Data not found</center>",
                "processing": '<center><i class="fa fa-refresh fa-spin fa-3x fa-fw" style="font-size:16pt;"></i></center>',

            },
            "ordering": false,            
            'searching': false,
            "paging": false,

        });
        $('#DataTable').wrap("<div class='table-responsive'></div>");
        $('#DeleteSelected').fadeOut(0);
        $("#CheckAll").click(function() {
            $('#DataTable').find('input:checkbox').not(this).prop('checked', this.checked);
            CheckTotalCheckedDelete();
        });
    });

    function Refresh() {
        table.ajax.reload(null, false);
    }

    function CheckTotalCheckedDelete() {
        var TotalOfCheckBoxRow = $('input#CheckboxRow').length;
        var TotalOfChecked = $('input#CheckboxRow:checked').length;
        if (TotalOfChecked > 0) {
            $('#DeleteSelected').fadeIn(0);
            if (TotalOfCheckBoxRow == TotalOfChecked) {
                $('#CheckAll').prop('checked', true);
            } else {
                $('#CheckAll').prop('checked', false);
            }
        } else {
            $('#DeleteSelected').fadeOut(0);
            $('#CheckAll').prop('checked', false);
        }
    }

    function Reset() {
        if (save_type == "Update") {
            ResetForm();
        }
        setTimeout(() => {}, 0);
    }

    function ResetValidation() {
        $('#form').find('input').popover('dispose');
        $('#form').find('select').popover('dispose');
    }

    function ResetForm() {
        $('#form')[0].reset();
        ResetValidation();
        setTimeout(() => {}, 0);
    }

    function Add() {
        Reset();
        save_type = "Add";
        $("#modal-form").modal('show');
    }

    function Edit(value) {
        ResetForm();
        $.ajax({
            url: module_api + "/where",
            type: 'GET',
            data: {
                id: value,
            },
            success: function(data) {
                if (data.error == 0) {
                    var d = data.data[0];
                    $('[name=id]').val(d.id);
                    $('[name=kode_menu]').val(d.kode_menu);
                    $('[name=name_menu]').val(d.name_menu);
                    $('[name=url]').val(d.url);
                    $('[name=rel]').val(d.rel);
                    $('[name=sequence]').val(d.sequence);
                    if (d.icon) {
                        $('input[name=icon]').filter('[value='+d.icon+']').prop('checked', true);
                    }
                    $("#modal-form").modal('show');
                    save_type = "Update";
                } else {
                    toastr.warning(data.message);
                }
            },
            error: function(data) {
                toastr.error("Error Edit Data");
            }
        });
    }
    
    function Detail(value){
        $.ajax({
            url:module_api+"/where",
            type:'GET',
            data:{
                id:value,
            },
            success: function(data){
                if (data.error == 0) {
                    $("#modal-detail").modal('show');
                    var d = data.data[0];
                    $('.kode_menu').text(d.kode_menu);
                    $('.name_menu').text(d.name_menu);
                    $('.sequence').text(d.sequence);
                    $('.url').text(d.url);
                    if (d.icon) {
                        $('.icon').html("<i class='material-icons'>"+d.icon+"</i>");
                    }else{
                        $('.icon').html("");

                    }
                    $('.parent_menu_name').text(d.parent_menu.name_menu);
                    $('.allow_login').text((d.allow_login == 1) ? "Ya" : "Tidak");
                    $('.created_by_name').text((d.created_by_user) ? d.created_by_user.name : null);
                    $('.created_date').text(d.created_date);
                    $('.last_modified_by_name').text((d.last_modified_by_user) ? d.last_modified_by_user.name : null);
                    $('.last_modified_date').text(d.last_modified_date);
                }else{
                    toastr.warning(data.message);
                }
            },
            error:function(data){
                toastr.error("Error Detail Data");
            }
        });
    }

		function Delete(value) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Delete Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {		
					$.ajax({
						url:module_api+"/"+value,
						method:'delete',
						success:function(data){
							if (data.error == 0) {
                                location.reload();
							}else{
								toastr.warning(data.message);
							}
	                        swal.close();
						},
						error:function(data){
                            toastr.error('Error Delete Data');
	                        swal.close();
						}
					});
				}
			});
		}

		function DeleteSelected(){
            var ValChecked = [];
            $('#CheckboxRow:checked').each(function() {
                ValChecked.push($(this).attr('data-id'));
            });
            Swal.fire({
                title: 'Are you sure?',
                text: "Delete Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {			
					$.ajax({
						url: module_api+"/delete_selected",
						method:'post',
						data:{id:ValChecked},
						success:function(data){
							if (data.error == 0) {
                                location.reload();
							}else{
								toastr.warning(data.message);
							}
	                        swal.close();
						},
						error:function(data){
                            toastr.error("Error Delete Selected Data");
	                        swal.close();
						}
					});
				}
			})
		}
</script>
@endsection