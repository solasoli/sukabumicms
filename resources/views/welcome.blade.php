{{-- @extends('layouts.app', ['class' => 'off-canvas-sidebar', 'title' => __('Material Dashboard')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
          <h1 class="text-white text-center">{{ __('Welcome to Material Dashboard FREE Laravel Live Preview.') }}</h1>
      </div>
  </div>
</div>
@endsection --}}
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Template Mo">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <title>Bank Inovasi Pemerintah Kabupaten Sukabumi</title>
    <!--

ART FACTORY

https://templatemo.com/tm-537-art-factory

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/templatemo-art-factory.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/owl-carousel.css') }}">

</head>

<body>

    <!-- ***** Preloader Start ***** -->
    {{-- <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div> --}}
    <!-- ***** Preloader End ***** -->


    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="#" class="logo" style="font-size: 12px">
                            <img src="{{ asset('images/kabupaten-sukabumi.png') }}" width="40px">
                            Bank Inovasi</a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="/" class="active">Beranda</a></li>
                            <li class="scroll-to-section"><a href="/login">Masuk</a></li>
                            <li class="scroll-to-section"><a href="/register">Daftar</a></li>
                            {{-- <li class="scroll-to-section"><a href="#frequently-question">Frequently Questions</a></li>
                            <li class="submenu">
                                <a href="javascript:;">Drop Down</a>
                                <ul>
                                    <li><a href="">About Us</a></li>
                                    <li><a href="">Features</a></li>
                                    <li><a href="">FAQ's</a></li>
                                    <li><a href="">Blog</a></li>
                                </ul>
                            </li>
                            <li class="scroll-to-section"><a href="#contact-us">Contact Us</a></li> --}}
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->


    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcome">

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row">
                    <div class="left-text col-lg-6 col-md-6 col-sm-12 col-xs-12"
                        data-scroll-reveal="enter left move 30px over 0.6s after 0.4s" style="margin-top: 100px">
                        <h1>Wilujeng Tepang</h1>
                        <h2><strong> Bank Inovasi Pelayanan Publik Kabupaten Sukabumi</strong></h2>
                        <br>
                        <h4 style="font-style: italic">Mewujudkan Inovasi Pelayanan Publik yang Responsif dan Adaptif
                        </h4>
                        <p>Pendaftaran Inovasi Kabupaten Sukabumi</p>
                        <a href="/register" class="main-button-slider">Daftar Sekarang!</a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                        data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                        <img src="{{ asset('/images/slider-icon.png') }}" class="rounded img-fluid d-block mx-auto"
                            alt="First Vector Graphic">
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    <section class="section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12"
                    data-scroll-reveal="enter left move 30px over 0.6s after 0.4s">
                    <img src="{{ asset('/images/left-image.png') }}" class="rounded img-fluid d-block mx-auto"
                        alt="App">
                </div>
                <div class="right-text col-lg-5 col-md-12 col-sm-12 mobile-top-fix">
                    <div class="left-heading">
                        <h5>Persyaratan Inovasi</h5>
                    </div>
                    <div class="left-text">
                        <ol class="list-group">
                            <li class="list-group-item">1. Memenuhi seluruh kriteria Inovasi.</li>
                            <li class="list-group-item">2. Relevan dengan salah satu Kategori Inovasi</li>
                            <li class="list-group-item">3. Menggunakan Judul Inovasi yang menggambarkan Inovasi dengan
                                memperhatikan norma dan kepantasan</li>
                            <li class="list-group-item">4. Tidak pernah mendapatkan Penghargaan sebagai TOP Inovasi pada
                                Kompetisi Inovasi Pelayanan Publik (KIPP) Tingkat Nasional</li>
                            <li class="list-group-item">5. Tidak pernah mendapatkan Penghargaan sebagai TOP 10 Kompetisi
                                Inovasi Jawa Barat</li>
                            <li class="list-group-item">6. Inovasi telah diimplementasikan minimal 6 (enam) bulan
                                dihitung mundur dari waktu penutupan pendaftaran Kompetisi</li>
                        </ol>
                        <br>
                        <a href="/register" class="main-button">Lihat Inovasi</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hr"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Big Item Start ***** -->
    {{-- <section class="section" id="about2">
        <div class="container">
            <div class="row">
                <div class="left-text col-lg-5 col-md-12 col-sm-12 mobile-bottom-fix">
                    <div class="left-heading">
                        <h5>Curabitur aliquam eget tellus id porta</h5>
                    </div>
                    <p>Proin justo sapien, posuere suscipit tortor in, fermentum mattis elit. Aenean in feugiat purus.</p>
                    <ul>
                        <li>
                            <img src="{{asset('/images/about-icon-01.png')}}">
                            <div class="text">
                                <h6>Nulla ultricies risus quis risus</h6>
                                <p>You can use this website template for commercial or non-commercial purposes.</p>
                            </div>
                        </li>
                        <li>
                            <img src="assets/images/about-icon-02.png" alt="">
                            <div class="text">
                                <h6>Donec consequat commodo purus</h6>
                                <p>You have no right to re-distribute this template as a downloadable ZIP file on any website.</p>
                            </div>
                        </li>
                        <li>
                            <img src="assets/images/about-icon-03.png" alt="">
                            <div class="text">
                                <h6>Sed placerat sollicitudin mauris</h6>
                                <p>If you have any question or comment, please <a rel="nofollow" href="https://templatemo.com/contact">contact</a> us on TemplateMo.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="right-image col-lg-7 col-md-12 col-sm-12 mobile-bottom-fix-big" data-scroll-reveal="enter right move 30px over 0.6s after 0.4s">
                    <img src="assets/images/right-image.png" class="rounded img-fluid d-block mx-auto" alt="App">
                </div>
            </div>
        </div>
    </section> --}}
    <!-- ***** Features Big Item End ***** -->


    <!-- ***** Features Small Start ***** -->
    <section class="section" id="services">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item service-item">
                        <h5 style="color: red;">26 Oktober 2021</h5>
                        <h4 class="service-title">Masa Pendaftaran Inovasi</h4>
                        <p>ZOOM MEETING</p>
                        <h6>SEKERTARIAT DAERAH KABUPATEN SUKABUMI</h6>
                    </div>
                    <div class="item service-item">
                        <h5 style="color: red;">27 Oktober 2021</h5>
                        <h4 class="service-title">Masa Pendaftaran Inovasi</h4>
                        <p>Pengajuan Online</p>
                        <h6>SEKERTARIAT DAERAH KABUPATEN SUKABUMI</h6>
                    </div>
                    <div class="item service-item">
                        <h5 style="color: red;">28 Oktober 2021</h5>
                        <h4 class="service-title">Masa Pendaftaran Inovasi</h4>
                        <p>ZOOM MEETING</p>
                        <h6>SEKERTARIAT DAERAH KABUPATEN SUKABUMI</h6>
                    </div>
                    <div class="item service-item">
                        <h5 style="color: red;">29 Oktober 2021</h5>
                        <h4 class="service-title">Masa Pendaftaran Inovasi</h4>
                        <p>ZOOM MEETING</p>
                        <h6>SEKERTARIAT DAERAH KABUPATEN SUKABUMI</h6>
                    </div>
                    <div class="item service-item">
                        <h5 style="color: red;">30 Oktober 2021</h5>
                        <h4 class="service-title">Masa Pendaftaran Inovasi</h4>
                        <p>ZOOM MEETING</p>
                        <h6>SEKERTARIAT DAERAH KABUPATEN SUKABUMI</h6>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme">
                    <div class="item service-item">
                        <img src="{{ asset('images/foto1.jpg') }}" alt="">
                    </div>
                    <div class="item service-item">
                        <img src="{{ asset('images/foto2.jpg') }}" alt="">
                    </div>
                    <div class="item service-item">
                        <img src="{{ asset('images/foto3.jpg') }}" alt="">
                    </div>
                    <div class="item service-item">
                        <img src="{{ asset('images/foto4.jpg') }}" alt="">
                    </div>
                    <div class="item service-item">
                        <img src="{{ asset('images/foto1.jpg') }}" alt="">
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->


    <!-- ***** Frequently Question Start ***** -->

    <!-- ***** Frequently Question End ***** -->


    <!-- ***** Contact Us Start ***** -->
    {{-- <section class="section" id="contact-us">
        <div class="container-fluid">
            <div class="row">
                <!-- ***** Contact Map Start ***** -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div id="map">
                      <!-- How to change your own map point
                           1. Go to Google Maps
                           2. Click on your location point
                           3. Click "Share" and choose "Embed map" tab
                           4. Copy only URL and paste it within the src="" field below
                    -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1197183.8373802372!2d-1.9415093691103689!3d6.781986417238027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfdb96f349e85efd%3A0xb8d1e0b88af1f0f5!2sKumasi+Central+Market!5e0!3m2!1sen!2sth!4v1532967884907" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <!-- ***** Contact Map End ***** -->

                <!-- ***** Contact Form Start ***** -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="contact-form">
                        <form id="contact" action="" method="post">
                          <div class="row">
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="name" type="text" id="name" placeholder="Full Name" required="" class="contact-field">
                              </fieldset>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <fieldset>
                                <input name="email" type="text" id="email" placeholder="E-mail" required="" class="contact-field">
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <textarea name="message" rows="6" id="message" placeholder="Your Message" required="" class="contact-field"></textarea>
                              </fieldset>
                            </div>
                            <div class="col-lg-12">
                              <fieldset>
                                <button type="submit" id="form-submit" class="main-button">Send It</button>
                              </fieldset>
                            </div>
                          </div>
                        </form>
                    </div>
                </div>
                <!-- ***** Contact Form End ***** -->
            </div>
        </div>
    </section> --}}
    <!-- ***** Contact Us End ***** -->


    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <p class="copyright">Copyright &copy; 2021 Bagian Organisasi Pemerintah Kabupaten Sukabumi
                    </p>
                </div>

            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="{{ asset('/js/jquery-2.1.0.min.js') }}"></script>

    <!-- Bootstrap -->
    <script src="{{ asset('/js/popper.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>

    <!-- Plugins -->
    <script src="{{ asset('/js/owl-carousel.js') }}"></script>
    <script src="{{ asset('/js/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('/js/imgfix.min.js') }}"></script>

    <!-- Global Init -->
    <script src="{{ asset('/js/custom.js') }}"></script>
    <style>
        * {
            box-sizing: border-box;
        }

        $max-img-width: 600px;
        $max-img-height: 400px;

        img {
            max-width: 100%;
            vertical-align: top;
        }

        .gallery {
            display: flex;
            margin: 10px auto;
            max-width: $max-img-width;
            position: relative;
            padding-top: $max-img-height/$max-img-width * 100%;

            @media screen and (min-width: $max-img-width) {
                padding-top: $max-img-height;
            }

            &__img {
                position: absolute;
                top: 0;
                left: 0;
                opacity: 0;
                transition: opacity 0.3s ease-in-out;
            }

            &__thumb {
                padding-top: 6px;
                margin: 6px;
                display: block;
            }

            &__selector {
                position: absolute;
                opacity: 0;
                visibility: hidden;

                &:checked {
                    +.gallery__img {
                        opacity: 1;
                    }

                    ~.gallery__thumb>img {
                        box-shadow: 0 0 0 3px #0be2f6;
                        ;
                    }
                }
            }


        }

    </style>
</body>

</html>
