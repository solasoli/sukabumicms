@extends('layouts.app', [
'firstMenu' => $firstMenu,
'secondMenu' => $secondMenu,
'titlePage' => __('Inovasi')])

@section('content')
<style>
    #DataTable tbody tr td:nth-child(1),
    #DataTable tbody tr td:nth-child(2) {
        text-align: center;
    }

    .btn,
    .btn.btn-default {
        color: black;
    }

    .card {
        margin-top: 0px;
    }

    .btn:hover {
        color: black;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <form id="form" class="form-horizontal">
            @csrf
            <input type="hidden" name="id" value="{{$inovasi->id}}">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-detail" width="100%">
                        <tr>
                            <th width="10%">Judul</th>
                            <th width="2">:</th>
                            <td><?php echo $inovasi->judul_inovasi ?></td>
                        </tr>
                        <tr>
                            <th>Kategori</th>
                            <th>:</th>
                            <td>{{$inovasi->klasifikasi_kategori->nama_klasifikasi_kategori}}</td>
                        </tr>
                        <tr>
                            <th>Nama Inovator</th>
                            <th>:</th>
                            <td>{{$inovasi->nama_inovator}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type='button' class='btn btn-primary' onclick="SimpanPerubahan();">Simpan Perubahan</button>
                    <button type='button' class='btn btn-danger'>Kirim Proposal</button>
                    <a class='btn btn-warning' href="{{url('inovasi')}}">Kembali ke List</a>
                    <div class="card" style="margin-top:30px;">
                        <h5 class="card-header card-header-info">Ringkasan</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 300</small>
                            <textarea name="ringkasan" class="ringkasan"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">1. Latar Belakang dan Tujuan (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="latar_belakang" class="latar_belakang"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">2. Kesesuaian Kategori (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="kesesuaian_kategori" class="kesesuaian_kategori"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">3. Kontribusi Terhadap Capaian SDGS/TPB (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="kontribusi" class="kontribusi"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">4. Deskripsi Inovasi (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="deskripsi_inovasi" class="deskripsi_inovasi"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">5. Inovatif (Keburuan, Nilai, Tambah, atau Keunikan) (15%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="inovatif" class="inovatif"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">6. Transferabilitas (Sifat dapat diterapkan pada konteks/tempat lain) (15%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="transferabilitas" class="transferabilitas"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">7. Sumber Daya (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="sdm" class="sdm"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">8. Strategi Keberlanjutan (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="strategi" class="strategi"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">9. Evaluasi (20%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="evaluasi" class="evaluasi"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">10. Keterlibatan Pemangku Kepentingan (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="keterlibatan" class="keterlibatan"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">11. Faktor Penentu (5%)</h5>
                        <div class="card-body">
                            <p class="card-title">Uraian Ringkasan Inovasi</p>
                            <small class="card-text">Maksimal 200 kata</small>
                            <textarea name="faktor_penentu" class="faktor_penentu"></textarea>
                        </div>
                    </div>
                    <div class="card">
                        <h5 class="card-header card-header-info">Dokumentasi Foto Inovasi</h5>
                        <div class="card-body">
                            <p class="card-title">Dokumentasi Foto yang menunjukan kegiatan dari Inovasi</p>
                            <small class="card-text">Maksimal 5 Foto</small>
                            <div class="col-12 row" style="margin-top:10px;">
                                <label>Dokumen 1</label>
                                <input type="file" name="dokumen_1" class="form-control" name="file" accept="image/*">
                            </div>
                            <div class="col-12 row" style="margin-top:10px;">
                                <label>Dokumen 2</label>
                                <input type="file" name="dokumen_2" class="form-control" name="file" accept="image/*">
                            </div>
                            <div class="col-12 row" style="margin-top:10px;">
                                <label>Dokumen 3</label>
                                <input type="file" name="dokumen_3" class="form-control" name="file" accept="image/*">
                            </div>
                            <div class="col-12 row" style="margin-top:10px;">
                                <label>Dokumen 4</label>
                                <input type="file" name="dokumen_4" class="form-control" name="file" accept="image/*">
                            </div>
                            <div class="col-12 row" style="margin-top:10px;">
                                <label>Dokumen 5</label>
                                <input type="file" name="dokumen_5" class="form-control" name="file" accept="image/*">
                            </div>
                        </div>
                    </div>
                    <a class='btn btn-warning' href="#">Back To Top</a>
                </div>
            </div>
        </form>
    </div>
</div>


@endsection
@section('script')
<script>
    var table = "";
    var module_api = "<?php echo url('/inovasi/proposal'); ?>";
    var save_type = "Add";
    var max300 = 300;
    var max200 = 200;
    var max100 = 200;
    var setting_toolbar = [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
    ];

    $(document).ready(function() {
        $('.ringkasan').summernote({
            toolbar: setting_toolbar,
            height: 100,
            focus: true,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            },
        });
        $('.latar_belakang').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.kesesuaian_kategori').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.kontribusi').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));
                }
            }
        });
        $('.deskripsi_inovasi').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.inovatif').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.transferabilitas').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.sdm').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.strategi').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.evaluasi').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.keterlibatan').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });
        $('.faktor_penentu').summernote({
            toolbar: setting_toolbar,
            height: 100,
            callbacks: {
                onKeydown: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onKeyup: function(e) {
                    var t = e.currentTarget.innerText;
                    var words = t.split(" ");
                    if (words.length >= max300) {
                        //delete key
                        if (e.keyCode != 8)
                            e.preventDefault();
                        // add other keys ...
                    }
                },
                onPaste: function(e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData ||
                        window.clipboardData).getData('Text');
                    e.preventDefault();
                    var all = t + bufferText;
                    var words = all.split(" ");
                    var array = words.slice(0, max300)
                    document.execCommand('insertText', false, array.join(" "));

                }
            }
        });

        setTimeout(() => {
            $(".ringkasan").summernote("code", "{{$inovasi->ringkasan}}");
            $(".latar_belakang").summernote("code", "{{$inovasi->latar_belakang}}");
            $(".kesesuaian_kategori").summernote("code", "{{$inovasi->kesesuaian_kategori}}");
            $(".kontribusi").summernote("code", "{{$inovasi->kontribusi}}");
            $(".deskripsi_inovasi").summernote("code", "{{$inovasi->deskripsi_inovasi}}");
            $(".inovatif").summernote("code", "{{$inovasi->inovatif}}");
            $(".transferabilitas").summernote("code", "{{$inovasi->transferabilitas}}");
            $(".sdm").summernote("code", "{{$inovasi->sdm}}");
            $(".strategi").summernote("code", "{{$inovasi->strategi}}");
			$(".evaluasi").summernote("code", "{{$inovasi->evaluasi}}"); //9
			$(".metode").summernote("code", "{{$inovasi->metode}}"); //9
			$(".hasil").summernote("code", "{{$inovasi->hasil}}"); //9
			$(".penyesuaian").summernote("code", "{{$inovasi->penyesuaian}}"); //9
            $(".keterlibatan").summernote("code", "{{$inovasi->keterlibatan}}");
            $(".faktor_penentu").summernote("code", "{{$inovasi->faktor_penentu}}");
        }, 300);
    });

    function SimpanPerubahan() {
        Swal.fire({
            title: 'Simpan Perubahan?',
            text: "Pastikan data yang diisi sudah benar!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: module_api,
                    method: 'post',
                    data:new FormData($('#form')[0]),
                            contentType: false,
                            processData: false,
                            async: false,
                            dataType: 'json',
                    success: function(data) {
                        if (data.error == 0) {
                            toastr.success(data.message);
                        } else {
                            toastr.warning(data.message);
                        }
                        swal.close();
                    },
                    error: function(data) {
                        toastr.error('Error Delete Data');
                        swal.close();
                    }
                });
            }
        });
    }

</script>
@endsection