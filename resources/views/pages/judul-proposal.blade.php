@extends('layouts.app', ['titlePage' => __('Input Proposal')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Data Proposal</h4>
                    <p class="card-category">Isi Data Proposal Anda</p>
                </div>
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Judul Proposal</label>
                            <input type="text" class="form-control" id="judulProposal">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Kategori</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>-- Pilih Kategori --</option>
                                <option>Kategori 1</option>
                                <option>Kategori 2</option>
                                <option>Kategori 3</option>
                                <option>Kategori 4</option>
                                <option>Kategori 5</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Elemen Masyarakat</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>-- Pilih Elemen --</option>
                                <option>Kepala Daerah</option>
                                <option>OPD</option>
                                <option>Masyarakat</option>

                            </select>
                        </div>

                        {{-- <div class="form-group">
                            <label for="exampleFormControlSelect2">Example multiple select</label>
                            <select multiple class="form-control" id="exampleFormControlSelect2">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="exampleFormControlTextarea1">Example textarea</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div> --}}
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Selanjutnya</button>
                        </div>
                    </form>
                    {{-- <div id="typography">
                        <div class="card-title">
                            <h2>Typography</h2>
                        </div>
                        <div class="row">
                            <div class="tim-typo">
                                <h1>
                                    <span class="tim-note">Header 1</span>The Life of Material Dashboard
                                </h1>
                            </div>
                            <div class="tim-typo">
                                <h2>
                                    <span class="tim-note">Header 2</span>The Life of Material Dashboard
                                </h2>
                            </div>
                            <div class="tim-typo">
                                <h3>
                                    <span class="tim-note">Header 3</span>The Life of Material Dashboard
                                </h3>
                            </div>
                            <div class="tim-typo">
                                <h4>
                                    <span class="tim-note">Header 4</span>The Life of Material Dashboard
                                </h4>
                            </div>
                            <div class="tim-typo">
                                <h5>
                                    <span class="tim-note">Header 5</span>The Life of Material Dashboard
                                </h5>
                            </div>
                            <div class="tim-typo">
                                <h6>
                                    <span class="tim-note">Header 6</span>The Life of Material Dashboard
                                </h6>
                            </div>
                            <div class="tim-typo">
                                <p>
                                    <span class="tim-note">Paragraph</span>
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers. I understand culture. I am the nucleus. I think that’s a
                                    responsibility that I have, to push possibilities, to show people, this is the level
                                    that things could be at.
                                </p>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Quote</span>
                                <blockquote class="blockquote">
                                    <p>
                                        I will be the leader of a company that ends up being worth billions of dollars,
                                        because I got the answers. I understand culture. I am the nucleus. I think that’s a
                                        responsibility that I have, to push possibilities, to show people, this is the level
                                        that things could be at.
                                    </p>
                                    <small>
                                        Kanye West, Musician
                                    </small>
                                </blockquote>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Muted Text</span>
                                <p class="text-muted">
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers...
                                </p>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Primary Text</span>
                                <p class="text-primary">
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers... </p>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Info Text</span>
                                <p class="text-info">
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers... </p>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Success Text</span>
                                <p class="text-success">
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers... </p>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Warning Text</span>
                                <p class="text-warning">
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers...
                                </p>
                            </div>
                            <div class="tim-typo">
                                <span class="tim-note">Danger Text</span>
                                <p class="text-danger">
                                    I will be the leader of a company that ends up being worth billions of dollars, because
                                    I got the answers... </p>
                            </div>
                            <div class="tim-typo">
                                <h2>
                                    <span class="tim-note">Small Tag</span>
                                    Header with small subtitle
                                    <br>
                                    <small>Use "small" tag for the headers</small>
                                </h2>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <script>

    </script>
@endsection
