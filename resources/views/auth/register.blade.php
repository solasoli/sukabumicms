@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'titlePage' => 'Register', 'title' => __('Daftar Akun')])

@section('content')
<div class="container" style="height: auto;">
    <div class="row align-items-center">
        <div class="col-lg-6 col-md-6 col-sm-8 ml-auto mr-auto">
            <form id="form" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="card card-login card-hidden mb-3">
                    <div class="card-header card-header-primary text-center">
                        <h4 class="card-title"><strong>{{ __('Daftar Akun Baru ') }}</strong></h4>
                    </div>
                    <div class="card-body ">
                        {{-- <p class="card-description text-center">{{ __('Or Be Classical') }}</p> --}}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bmd-form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">face</i>
                                            </span>
                                        </div>
                                        <input type="text" name="name" class="form-control" placeholder="Nama Penanggung Jawab" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bmd-form-group mt-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">email</i>
                                            </span>
                                        </div>
                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group mt-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">fax</i>
                                            </span>
                                        </div>
                                        <input type="text" name="no_telp" class="form-control" placeholder="No. Telp/Handphone" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bmd-form-group mt-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                        </div>
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Kata Sandi" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group mt-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                        </div>
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Konfirmasi Kata Sandi" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bmd-form-group mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">apartment</i>
                                    </span>
                                </div>
                                <select class="form-control" name="perangkat_daerah_id">
                                    <option value="">Pilih Jenis Pengguna</option>
                                    @foreach ($listPerangkatDaerah as $item)
                                        <option value="{{$item->id}}">{{$item->nama_perangkat_daerah}}</option>
                                    @endforeach
                                </select>
                                <input type="text" name="instansi" id="instansi" class="form-control" placeholder="Instansi" style="display:none">
                                <div class="checkbox" style="padding-top:10px;">
                                    <label>
                                        <input type="checkbox" value="1" id="lainnya" name="lainnya">
                                        Lainnya
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="bmd-form-group mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">group_add</i>
                                    </span>
                                </div>
                                <select class="form-control" name="element_masyarakat_id" required>
                                    <option value="">Pilih Jenis Pengguna</option>
                                    @foreach ($listElementMasyarakat as $item)
                                        <option value="{{$item->id}}">{{$item->nama_element_masyarakat}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer justify-content-center">
                        <button type="submit" class="btn btn-primary btn-link btn-lg">Buat Akun</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#form').on("submit", function(event) {
            event.preventDefault();
            ResetValidation();
            Swal.fire({
                title: 'Are you sure?',
                text: "Save Data",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
            }).then((result) => {
                console.log(result)
                if (result.value) {
                    setTimeout(() => {
                        $.ajax({
                            url: "<?php echo url('/register'); ?>",
                            data: new FormData($('#form')[0]),
                            type: 'post',
                            contentType: false,
                            processData: false,
                            async: false,
                            dataType: 'json',
                            success: function(data) {
                                if (data.error == 0) {
                                    $('#form')[0].reset();
                                    ResetValidation();
                                    toastr.info(data.message);
                                } else if (data.error == 1) {
                                    setTimeout(() => {
                                        $.each(data.error_validation, function(key, value) {
                                            ValidationPopover('[name=' + key + ']', value);
                                        });
                                    }, 0);
                                    toastr.warning(data.message);
                                } else {
                                    toastr.warning(data.message);
                                }
                                swal.close();
                            },
                            error: function(data) {
                                swal.close();
                                toastr.error("Error Register");
                            }
                        });
                    }, 150);
                }
            })
        });

        $("#lainnya").click(function() {
            if (this.checked == true) {
                $("[name=instansi]").fadeIn(0);
                $("[name=instansi]").attr("required", true);
                $("[name=perangkat_daerah_id]").fadeOut(0);
                $("[name=perangkat_daerah_Id]").attr("required", false);
            }else{
                $("[name=instansi]").fadeOut(0);
                $("[name=instansi]").attr("required", false);
                $("[name=perangkat_daerah_id]").fadeIn(0);
                $("[name=perangkat_daerah_Id]").attr("required", true);
            }
        });
    });

    function ResetValidation() {
        $('#form').find('input').popover('dispose');
        $('#form').find('select').popover('dispose');
    }

</script>
@endsection
