<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 200)->index();
            $table->string('email', 200);
            $table->enum('status', ['Aktif', 'Nonaktif']);
            $table->string('file_foto', 20)->nullable();
            $table->integer("users_level_id", false, false)->length(11)->nullable()->index();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('no_telp', 50)->nullable();
            $table->string('instansi', 250)->nullable()->index();
            $table->integer("perangkat_daerah_id", false, false)->length(11)->nullable()->index();
            $table->integer("element_masyarakat_id", false, false)->length(11)->nullable()->index();
            $table->string('password');
            $table->boolean("is_konfirmasi")->default(true)->index()->index();
            $table->integer("is_konfirmasi_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("is_konfirmasi_date")->nullable();
            $table->integer("created_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("created_date")->nullable();
            $table->integer("last_modified_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("last_modified_date")->nullable();
            $table->boolean("is_deleted")->default(false)->index()->index();
            $table->integer("deleted_by", false, false)->length(11)->nullable();
            $table->dateTime("deleted_date")->nullable();
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
