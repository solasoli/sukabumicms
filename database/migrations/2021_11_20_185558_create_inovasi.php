<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInovasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inovasi', function (Blueprint $table) {
            $table->id();
            $table->string('judul_inovasi', 250)->index();
            $table->string('nama_inovator', 100)->index();
            $table->integer('klasifikasi_kategori_id', false, false)->length(11)->nullable()->index();
            $table->integer('users_id', false, false)->length(11)->nullable()->index();
            $table->date("tanggal_inisiasi")->nullable();
            $table->text("ringkasan")->nullable();
            $table->text("latar_belakang")->nullable();
            $table->text("kesesuaian_kategori")->nullable();
            $table->text("kontribusi")->nullable();
            $table->text("deskripsi_inovasi")->nullable();
            $table->text("inovatif")->nullable();
            $table->text("transferabilitas")->nullable();
            $table->text("sdm")->nullable();
            $table->text("strategi")->nullable();
                $table->text("evaluasi")->nullable();
                $table->text("metode")->nullable();
                $table->text("hasil")->nullable();
                $table->text("penyesuaian")->nullable();
            $table->text("keterlibatan")->nullable();
            $table->text("faktor_penentu")->nullable();
            $table->integer('created_by', false, false)->length(11)->nullable()->index();
            $table->dateTime("created_date")->nullable();
            $table->integer('last_modified_by', false, false)->length(11)->nullable()->index();
            $table->dateTime("last_modified_date")->nullable();
            $table->boolean("is_deleted")->default(false)->index();
            $table->integer('deleted_by', false, false)->length(11)->nullable()->index();
            $table->dateTime("deleted_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inovasi');
    }
}
