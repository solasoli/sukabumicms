<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_access', function (Blueprint $table) {
            $table->id();
            $table->integer("users_level_id", false, false)->length(11)->nullable()->index();
            $table->integer("users_menu_id", false, false)->length(11)->nullable()->index();
            $table->enum('access', ['show', 'add', 'edit', 'detail', 'delete'])->index();
            $table->integer("created_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("created_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_access');
    }
}
