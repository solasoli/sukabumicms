<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersLevelGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_level_group', function (Blueprint $table) {
            $table->id();
            $table->integer('users_id', false, false)->length(11)->nullable()->index();
            $table->integer('users_level_id', false, false)->length(11)->nullable()->index();
            $table->integer("created_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("created_date")->nullable();
            $table->integer("last_modified_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("last_modified_date")->nullable();
            $table->boolean("is_deleted")->default(false)->index();
            $table->integer("deleted_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("deleted_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_level_group');
    }
}
