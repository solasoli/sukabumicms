<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_menu', function (Blueprint $table) {
            $table->id();
            $table->string('kode_menu', 50)->index();
            $table->string('name_menu', 100)->index();
            $table->string('url', 200)->index();
            $table->string('icon', 100)->nullable();
            $table->integer('rel', false, false)->length(11)->index();
            $table->integer('sequence', false, false)->length(11)->index();
            $table->integer("created_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("created_date")->nullable();
            $table->integer("last_modified_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("last_modified_date")->nullable();
            $table->boolean("is_deleted")->default(false)->index();
            $table->integer("deleted_by", false, false)->length(11)->nullable()->index();
            $table->dateTime("deleted_date")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_menu');
    }
}
