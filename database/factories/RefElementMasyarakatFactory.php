<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\RefElementMasyarakat;

class RefElementMasyarakatFactory extends Factory
{
    /**
     * Define the model's default state.
     * 
     
     *
     * @return array
     */
    protected $model = RefElementMasyarakat::class;

    public function definition()
    {
        return [
            'nama_element_masyarakat' => $this->faker->jobTitle,
            'created_by' => 1,
            'created_date' => now(),
        ];
    }
}
