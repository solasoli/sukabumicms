<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\RefPerangkatDaerah;

class RefPerangkatDaerahFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = RefPerangkatDaerah::class;

    public function definition()
    {
        return [
            'nama_perangkat_daerah' => $this->faker->jobTitle,
            'created_by' => 1,
            'created_date' => now(),
        ];
    }
}
