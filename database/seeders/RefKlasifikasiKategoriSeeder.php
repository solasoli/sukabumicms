<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RefKlasifikasiKategori;

class RefKlasifikasiKategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RefKlasifikasiKategori::factory(10)->create();
    }
}
