<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_level')->insert([
            'name_level' => 'Admin',
            'sequence' => 1,
            'created_date' => now(),
        ]);
        DB::table('users_level')->insert([
            'name_level' => 'Operator',
            'sequence' => 2,
            'created_date' => now(),
        ]);
        DB::table('users_level')->insert([
            'name_level' => 'Pimpinan',
            'sequence' => 3,
            'created_date' => now(),
        ]);
        DB::table('users_level')->insert([
            'name_level' => 'User',
            'sequence' => 4,
            'created_date' => now(),
        ]);
    }
}
