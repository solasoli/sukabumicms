<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin Admin',
            'email' => 'admin@material.com',
            'status' => 'Aktif',
            'email_verified_at' => now(),
            'users_level_id' => 1,
            'password' => Hash::make('secret'),
            'created_date' => now(),
        ]);
    }
}
